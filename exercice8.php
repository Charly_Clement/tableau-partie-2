<!doctype html>
<html lang="fr">
<head>
    <!-- CodeColliders 2020 - Exercice PHP -->
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tableaux PHP - Partie 2 - Exercice</title>
</head>
<body>
<h1>Exercices Tableaux PHP</h1>
<?php

$resultats = [
    "Simon" => [
        "age" => 25,
        "tempsCourse" => 30,
    ],
    "Jean" => [
        "age" => 35,
        "tempsCourse" => 45,
    ],
    "Claire" => [
        "age" => 19,
        "tempsCourse" => 90,
    ],
    "Anne-Lise" => [
        "age" => 18,
        "tempsCourse" => 68,
    ],
    "Lucie" => [
        "age" => 43,
        "tempsCourse" => 35,
    ],
    "Bernard" => [
        "age" => 50,
        "tempsCourse" => 40,
    ],
];

/**
 * Exercice 8:
 * Le tableau $resultats contient les résultats d'une course à pied.
 * Calculer et afficher le temps de course moyen pour
 * les participants ayant plus de 30 ans.
 */
echo '<h2>Exercice 8</h2>';

    foreach ($resultats as $nom => $personne) {
        if ($personne['age'] > 30) {
            $table[] = $personne['tempsCourse'];
        }
    }
    $somme = array_sum($table);

    $taille = count($table);

    $moyenne = $somme / $taille;

    echo 'tempsCourse'.$moyenne;


?>
</body>
</html>
